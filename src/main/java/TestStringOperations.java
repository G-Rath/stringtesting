import java.io.Console;
import java.util.Random;

import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Created by Gareth.Jones on 10/08/2015.
 */
public class TestStringOperations
{
	public static final String ANSI_RESET = "\u001B[0m";
	public static final String ANSI_BLACK = "\u001B[30m";
	public static final String ANSI_RED = "\u001B[31m";
	public static final String ANSI_GREEN = "\u001B[32m";
	public static final String ANSI_YELLOW = "\u001B[33m";
	public static final String ANSI_BLUE = "\u001B[34m";
	public static final String ANSI_PURPLE = "\u001B[35m";
	public static final String ANSI_CYAN = "\u001B[36m";
	public static final String ANSI_WHITE = "\u001B[37m";

	private SecureRandom random = new SecureRandom();

	public String genRandomString()
	{
		return new BigInteger( 100, random ).toString( 32 );
	}
	/*
	Fact 1: "string" + "" = "string"
	Fact 2: "hello " + "world" = "hello world"
	Fact 3: substring( "hello world", 4 ) = "hello"
	Fact 4: substring( "hello world", RAND-NUMBER-GREATER-THAN-LENGTH ) = null
	 */

	public int numberOfTests = 1000;

	public static void main( String[] args )
	{
		new TestStringOperations();
	}

	public TestStringOperations()
	{
		int testNumber = 0;

		int fact1FailCount = 0;
		int fact2FailCount = 0;
		int fact3FailCount = 0;
		int fact4FailCount = 0;
		int fact5FailCount = 0;

		for( int i = 1; i < numberOfTests + 1; i++ )
		{
			System.out.println( ANSI_YELLOW + "test #" + i + " -- start" + ANSI_RESET );
//			System.out.println( "-------------------------------" );
			fact1FailCount += testFact1();
//			System.out.println( "-------------------------------" );
			fact2FailCount += testFact2();
//			System.out.println( "-------------------------------" );
			fact3FailCount += testFact3();
//			System.out.println( "-------------------------------" );
			fact4FailCount += testFact4();
//			System.out.println( "-------------------------------" );
			fact5FailCount += testFact5();
//			System.out.println( "-------------------------------" );
			System.out.println( ANSI_YELLOW +"test end" + ANSI_RESET );

			System.out.print( "\n" );
//			System.out.print( "\n" );
//			System.out.print( "\n" );
		}

		System.out.println( "-------------------------------" );
		System.out.println( "All tests finished: " + numberOfTests + " tests run" );
		System.out.println( "Results: " );
		System.out.println( ( fact1FailCount > 0 ? ANSI_RED : ANSI_GREEN ) + "\t" + "Fact1: " + fact1FailCount + " tests failed" + ANSI_RESET );
		System.out.println( ( fact2FailCount > 0 ? ANSI_RED : ANSI_GREEN ) + "\t" + "Fact2: " + fact2FailCount + " tests failed" + ANSI_RESET );
		System.out.println( ( fact3FailCount > 0 ? ANSI_RED : ANSI_GREEN ) + "\t" + "Fact3: " + fact3FailCount + " tests failed" + ANSI_RESET );
		System.out.println( ( fact4FailCount > 0 ? ANSI_RED : ANSI_GREEN ) + "\t" + "Fact4: " + fact4FailCount + " tests failed" + ANSI_RESET );
		System.out.println( ( fact5FailCount > 0 ? ANSI_RED : ANSI_GREEN ) + "\t" + "Fact5: " + fact5FailCount + " tests failed" + ANSI_RESET );
		System.out.println( "-------------------------------" );
	}

	public int testFact1()
	{
		StringOperations operations = new StringOperations();

		boolean testFailed = false;

		for( int i = 0; i < 100; i++ )
		{
			String randomString = genRandomString();

			if( !randomString.equals( operations.addString( randomString, "" ) ) )
			{
				System.out.println( ANSI_RED + "\t" + "error appending blank string to " + randomString + ANSI_RESET );
				testFailed = true;
			}
		}

		if( !testFailed )
		{
			System.out.println( ANSI_GREEN + "\t" + "Fact1 is correct" + ANSI_RESET );
			return 0;
		}
		else
			return 1;
	}

	public int testFact2()
	{
		StringOperations operations = new StringOperations();

		boolean testFailed = false;

		if( !operations.addString( "hello ", "world" ).equals( "hello world" ) )
		{
			System.out.println( ANSI_RED + "\t" + "error adding \"world\" to \"hello \"" + ANSI_RESET );
			testFailed = true;
		}

		if( !testFailed )
		{
			System.out.println( ANSI_GREEN + "\t" + "Fact1 is correct" + ANSI_RESET );
			return 0;
		}
		else
			return 1;
	}

	public int testFact3()
	{
		StringOperations operations = new StringOperations();

		boolean testFailed = false;

		if( !operations.substring( "hello world", 0, 5 ).equals( "hello" ) )
		{
			System.out.println( ANSI_RED + "\t" + "error incorrect substring" + ANSI_RESET );
			testFailed = true;
		}

		if( !testFailed )
		{
			System.out.println( ANSI_GREEN + "\t" + "Fact3 is correct" + ANSI_RESET );
			return 0;
		}
		else
			return 1;
	}

	public int testFact4()
	{
		Random randomGenerator = new Random();
		StringOperations operations = new StringOperations();

		boolean testFailed = false;

		for( int i = 0; i < 1000; i++ )
		{
			String randomString = genRandomString();
			int randomInt = randomGenerator.nextInt( 1000 ) + randomString.length();

			if( operations.substring( randomString, randomInt ) != null )
			{
				if( !testFailed )
				{
					System.out.println( ANSI_RED + "\t"  + "Fact4 failed:" + ANSI_RESET );
				}

				System.out.println( ANSI_RED + "\t\t" + "#" + i + ": incorrect substringing " + randomString + " at " + randomInt + ANSI_RESET );
				testFailed = true;
			}

		}

		if( !testFailed )
		{
			System.out.println( ANSI_GREEN + "\t" + "Fact4 is correct" + ANSI_RESET );
			return 0;
		}
		else
		{
			return 1;
		}
	}

	public int testFact5()
	{
		Random randomGenerator = new Random();
		StringOperations operations = new StringOperations();

		boolean testFailed = false;

		for( int i = 0; i < 1000; i++ )
		{
			String randomString = genRandomString();
			int randomStartInt = randomGenerator.nextInt( randomString.length() );
			int randomEndInt = randomGenerator.nextInt( randomString.length() );

			if( !operations.substring( randomString, randomStartInt, randomEndInt ).equals( operations.substring( randomString, randomEndInt, randomStartInt ) ) )
			{
				if( !testFailed )
				{
					System.out.println( ANSI_RED + "\t"  + "Fact5 failed:" + ANSI_RESET );
				}

				System.out.println( ANSI_GREEN + "\t\t" + "#" + i + ": incorrect substring swap - " + randomString + ", " + randomStartInt + ", " + randomEndInt + ANSI_RESET );
				testFailed = true;
			}
		}

		if( !testFailed )
		{
			System.out.println( ANSI_GREEN + "\t" + "Fact5 is correct" + ANSI_RESET );
			return 0;
		}
		else
			return 1;
	}
}
