/**
 * Created by Gareth.Jones on 10/08/2015.
 */
public class StringOperations
{
	public StringOperations()
	{

	}

	public String addString( String firstString, String secondString )
	{
		return firstString + secondString;
	}

	/**
	 * substrings the {@code String} provided from the given start position. This method calls {@link StringOperations#substring(String, int, int)} passing {@code -1} as the {@code endPos} value.
	 *
	 * @param string   the {@code String} that is to be substring-ed.
	 * @param startPos the starting position in the string to substring from.
	 *
	 * @return substring-ed {@code String} substring-ed from the given starting position, else {@code NULL} if the given starting position was invalid.
	 */
	public String substring( String string, int startPos )
	{
		return substring( string, startPos, -1 );
	}

	/**
	 * substrings the {@code String} provided from the given start position to the given end position. If {@code -1} is passed then the end position
	 * is ignored and only the start position is used, going from the start position to the end of the {@code String}.
	 *
	 * @param string the {@code String}
	 * @param startPos
	 * @param endPos
	 *
	 * @return
	 */
	public String substring( String string, int startPos, int endPos )
	{
		if( startPos > string.length()) //If the start position is greater than the length of the string, we can't substring, so return null
			return null; //To simulate random test-fails use "startPos > string.length()" as the condition. For the correct output use "startPos >= string.length()" as the condition.
		else if( endPos > -1 && startPos > endPos ) //If an end position has been provided, and if the start position is greater than the end position.
		{
			int placeholder = startPos;
			startPos = endPos; //Swap the start position value with the end position value.
			endPos = placeholder;
		}

		if( endPos == -1 ) //If an end position has not been provided (based on if endPos == -1) then substring from the startPos to the end of the string.
		{
			return string.substring( startPos );
		}
		else //Else substring from the startPos to the endPos in the string.
		{
			return string.substring( startPos, endPos );
		}
	}
}
