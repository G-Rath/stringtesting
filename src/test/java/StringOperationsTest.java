import org.junit.Test;

import java.math.BigInteger;
import java.security.SecureRandom;
import java.util.Random;

import static org.junit.Assert.*;

/**
 * Created by Gareth.Jones on 17/08/2015.
 */
public class StringOperationsTest
{
	private SecureRandom random = new SecureRandom();

	public String genRandomString()
	{
		return new BigInteger( 100, random ).toString( 32 );
	}

	@Test
	public void testAddString() throws Exception
	{
		StringOperations operations = new StringOperations();

		for( int i = 0; i < 100; i++ )
		{
			String randomString = genRandomString();

			assertEquals( "Failed to add strings correctly", randomString, operations.addString( randomString, "" ) );
		}
	}

	@Test
	public void testSubstringCorrect() throws Exception
	{
		StringOperations operations = new StringOperations();
		assertEquals( "Failed to substring correctly, using string of 'hello world', startPos of 0 and endPos of 15", "hello", operations.substring( "hello world", 0, 10 ) );
	}

	@Test( expected = NullPointerException.class )
	public void testSubstringLengthTooLong() throws Exception
	{
		Random randomGenerator = new Random();
		StringOperations operations = new StringOperations();

		for( int i = 0; i < 10000; i++ )
		{
			String randomString = genRandomString();
			int randomInt = randomGenerator.nextInt( 1000 ) + randomString.length();

			operations.substring( randomString, randomInt ).length();
		}
	}

	@Test()
	public void testSubstringSwapPositions()
	{
		Random randomGenerator = new Random();
		StringOperations operations = new StringOperations();

		for( int i = 0; i < 10000; i++ )
		{
			String randomString = genRandomString();
			int randomStartInt = randomGenerator.nextInt( randomString.length() );
			int randomEndInt = randomGenerator.nextInt( randomString.length() );

			assertEquals( operations.substring( randomString, randomStartInt, randomEndInt ), operations.substring( randomString, randomEndInt, randomStartInt ) );
		}
	}
}